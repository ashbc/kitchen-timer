#[cfg(test)]
use crate::{*, KitchenTimerError::*};
#[cfg(test)]
use std::fmt::Debug;

#[cfg(test)]
fn expect_error<T: Debug, U: Debug, E>(result: Result<(impl Iterator<Item = T>, impl Iterator<Item = U>), E>) -> E {
    match result {
        Ok((x, y)) => {
            let a: Vec<T> = x.collect();
            let b: Vec<U> = y.collect();
            panic!("Expected error, got {:?}, {:?}", a, b)
        },
        Err(e) => e,
    }
}

#[test]
fn parse_zero_args() {
    let args: Vec<String> = vec![];
    let result = parse_args(args);
    let error = expect_error(result);
    match error {
        NoArguments => {}
        e => panic!("Expected NoArguments, got {}", e),
    }
}

#[test]
fn parse_one_arg() {
    let args: Vec<String> = vec![String::from("Eggs")];
    let result = parse_args(args);
    let error = expect_error(result);
    match error {
        UnevenArguments => {},
        e => panic!("Expected UnevenArguments, got {}", e),
    }
}

#[test]
fn parse_invalid_time() {
    let args: Vec<String> = vec![
        String::from("Eggs"),
        String::from("10bees"),
    ];
    let result = parse_args(args);
    let error = expect_error(result);
    match error {
        // opaquely check for a parse error
        // could expand this test to check each type of failure...
        InvalidTime(_) => {},
        e => panic!("Expected InvalidTime(_), got {}", e),
    }
}

#[test]
fn parse_args_correctly() -> Result<(), KitchenTimerError> {
    let args: Vec<String> = vec![
        String::from("Eggs"),
        String::from("2min"),
        String::from("More eggs"),
        String::from("20 sec"),
        String::from("sonic & knuckles"),
        String::from("1d2h3m5s"),
    ];
    let (labels, durations) = parse_args(args)?;
    let labels_v: Vec<String> = labels.collect();
    let durations_v: Vec<Duration> = durations.collect();
    assert_eq!(labels_v.len(), 3);
    assert_eq!(durations_v.len(), 3);

    Ok(())
}

#[test]
fn test_calc_rel_durations() {
    let ds = vec![
        20,
        30,
        45,
    ].into_iter().map(Duration::from_millis);
    let rel = calc_rel_durations(ds);
    assert_eq!(rel, vec![
        Duration::from_millis(15),
        Duration::from_millis(10),
        Duration::from_millis(20),
    ]);
}
