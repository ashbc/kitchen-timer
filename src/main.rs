mod main_tests;

use parse_duration::parse;
use std::{
    env,
    error::Error,
    fmt::{self, Display, Formatter},
    iter::Iterator,
    thread,
    time::Duration,
};

#[derive(Debug)]
enum KitchenTimerError {
    UnevenArguments,
    InvalidTime(parse::Error),
    NoArguments,
}
impl Error for KitchenTimerError {}
impl Display for KitchenTimerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnevenArguments => write!(f, "Number of arguments must be even."),
            Self::InvalidTime(e) => write!(f, "Parse error.\n{}", e),
            Self::NoArguments => write!(f, "No arguments specified."),
        }
    }
}
impl From<parse::Error> for KitchenTimerError {
    fn from(e: parse::Error) -> Self {
        KitchenTimerError::InvalidTime(e)
    }
}

fn main() {
    let mut args_iter = env::args();
    let exe = args_iter.next().unwrap();
    let args = args_iter.collect();
    let result = parse_args(args);
    match result {
        Ok((l, d)) => execute(l, d),
        Err(e) => eprintln!(
            "{}

USAGE:
{} LABEL DURATION ...

starts kitchen timers such that each timer ends at the same time.",
            e, exe
        ),
    };
}

fn parse_args(
    args: Vec<String>,
) -> Result<(impl Iterator<Item = String>, impl Iterator<Item = Duration>), KitchenTimerError> {
    if args.len() % 2 != 0 {
        return Err(KitchenTimerError::UnevenArguments);
    }
    if args.is_empty() {
        return Err(KitchenTimerError::NoArguments);
    }

    // split into odd/even position
    let (l, r): (Vec<_>, Vec<_>) = args.into_iter().enumerate().partition(|(i, _)| i % 2 == 0);

    // parse durations
    let durations: Result<Vec<Duration>, parse::Error> =
        r.into_iter().map(|(_, x)| parse(&x)).collect();

    Ok((l.into_iter().map(|(_, a)| a), durations?.into_iter()))
}

fn calc_rel_durations(ds: impl Iterator<Item = Duration>) -> Vec<Duration> {
    let mut durations_v: Vec<Duration> = ds.collect();
    durations_v.sort_unstable();
    // calculate the difference between each value
    durations_v.reverse();
    for i in 0..durations_v.len() {
        let (t0, t1) = (durations_v.get(i), durations_v.get(i + 1));
        durations_v[i] = match (t0, t1) {
            (Some(&a), Some(&b)) => a - b,
            (Some(&a), None) => a,
            (None, _) => panic!("array index out of bounds?"),
        };
    }
    durations_v
}

fn execute(labels: impl Iterator<Item = String>, durations: impl Iterator<Item = Duration>) {
    let rel_durations = calc_rel_durations(durations);
    for pair in labels.zip(rel_durations) {
        let (label, duration) = pair;
        println!("{}", label);
        thread::sleep(duration);
    }
}
